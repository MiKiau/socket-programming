/**
 * @file util.c
 * @brief Contains all utility functions. 
 * 
 * @author Žygimantas Kungelis
 */


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include "constants.h"
#include "util.h"

extern void readString(char *buffer, const int size)
{
    int n = 0;
    while (((buffer[n++] = getchar()) != '\n') && (n < size))
        ;
}

extern int readInt(void)
{
    char buffer[MAX_INPUT] = {0};
    readString(buffer, MAX_INPUT);
    return atoi(buffer);
}

extern void readPorts(uint16_t *portArray, const int *const threadCount)
{
    for (int i = 0; i < *threadCount; i++)
    {
        printf("#%d thread: which port to connect to? ", i + 1);
        portArray[i] = readInt();
    }
}