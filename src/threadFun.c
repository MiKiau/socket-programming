/**
 * @file commFun.c
 * @brief Contains all thread related functions. 
 * 
 * @author Žygimantas Kungelis
 */


#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#ifdef _WIN32
#include <winsock2.h>
#include <ws2tcpip.h>
#include <Windows.h>
#include <process.h>
#else
#include <sys/socket.h>
#include <arpa/inet.h>
#include <pthread.h>
#endif

#include "constants.h"
#include "commFun.h"
#include "util.h"
#include "main.h"

uint8_t moreThanOneThreads = 0;

#ifdef _WIN32

extern unsigned __stdcall threadCommunication(void *arg)
{
    SOCKET sockfd = createSocket();
    connectToServer(&sockfd, arg);
    communication(&sockfd, &moreThanOneThreads);
    sockClose(sockfd);
    free(arg);
    _endthreadex(0);
    return 0;
}

#else

extern void *threadCommunication(void *arg)
{
    SOCKET sockfd = createSocket();
    connectToServer(&sockfd, arg);
    communication(&sockfd, &moreThanOneThreads);
    sockClose(sockfd);
    free(arg);
    return NULL;
}

#endif

extern void startThreadCreating(const int *const threadCount,
                                const uint16_t *portArray)
{
    #ifdef _WIN32
    HANDLE hThreads[*threadCount];
    #else
    pthread_t thread = {0};
    #endif

    if (*threadCount > 1)
    {
        // If there will be more than one thread, set flag.
        moreThanOneThreads = 1;
    }

    for (int i = 0; i < *threadCount; i++)
    {
        // The best way to pass arguments to threads is through dynamic memory.
        int *arg = malloc(sizeof(*arg));
        if (arg == NULL)
        {
            perror("Couldn't allocate memory for thread argument.");
            exit(1);
        }
        *arg = portArray[i];
        #ifdef _WIN32
        hThreads[i] = (HANDLE)_beginthreadex(NULL, 0, threadCommunication, 
                                            (void *)arg, 0, NULL);
        #else
        pthread_create(&thread, NULL, threadCommunication, (void *)arg);
        #endif
    }
    #ifdef _WIN32
    // Wa
    WaitForMultipleObjects(*threadCount, hThreads, 1, INFINITE);
    for (int i = 0; i < *threadCount; i++)
    {
        DWORD exitCode = 0;
        GetExitCodeThread(hThreads[i], &exitCode);
        if (exitCode != 0) 
        {
            printf("%i thread's exit code: %ld\n", i + 1, exitCode);
        }
        CloseHandle(hThreads[i]);
    }
    #else
    pthread_exit(NULL);
    #endif
}