/**
 * @file main.c
 * @brief Contains driver function. 
 * 
 * @author Žygimantas Kungelis
 * @date 2020-12-07
 * @version 0.1v
 * 
 * TODO: cross-platform;
 * TODO: tidying project folder;
 * TODO: tests?
 */


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>

#ifdef _WIN32
#include <winsock2.h>
#include <ws2tcpip.h>
#endif

#include "constants.h"
#include "commFun.h"
#include "threadFun.h"
#include "util.h"
#include "main.h"


int main(void) {
    // Initialisation of Winsock - REQUIRED for windows
    sockInit();

    printf("How many threads do you want to create for port joining (max 20)? ");
    int threadCount = readInt();

    uint16_t portArray[MAX_THREAD_NUMBER] = {0};
    readPorts(portArray, &threadCount);

    startThreadCreating(&threadCount, portArray);
    return 0;
}