/**
 * @file commFun.c
 * @brief Contains all socket communication functions. 
 * 
 * @author Žygimantas Kungelis
 */


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>

#ifdef _WIN32
#include <winsock2.h>
#include <ws2tcpip.h>
#else
#include <unistd.h>         /* needed for close() */
#include <sys/socket.h>
#include <arpa/inet.h>
#endif

#include "constants.h"
#include "commFun.h"
#include "util.h"
#include "main.h"


extern void sockInit(void) 
{
    #ifdef _WIN32
        WSADATA wsa_data = {0};
        if (WSAStartup(MAKEWORD(2, 2), &wsa_data) != 0)
        {
            printf("Socket init failed with error: %d\n", WSAGetLastError());
            exit(1);
        }
    #else
        // Do nothing   
    #endif
}

extern SOCKET createSocket(void)
{
    // AF_INET - socket uses IPv6 internet protocol
    // SOCK_STREAM - makes the socket TCP/IP type
    // 0 - default value 0
    SOCKET sockfd = socket(AF_INET, SOCK_STREAM, 0);
    check(sockfd, "Socket creation failed");
    return sockfd;
}

extern void connectToServer(const SOCKET *const sockfd, 
                            const uint16_t *const port)
{
    struct sockaddr_in serverAddr = {0};
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_addr.s_addr = inet_addr(SERVER_IP_ADDRESS);
    serverAddr.sin_port = htons(*port);

    check(connect(*sockfd, 
                  (struct sockaddr *)&serverAddr, 
                  (socklen_t)sizeof(struct sockaddr)),
          "Socket connection failed");
}

SOCKET receiveMessage(const SOCKET *const sockfd, char *buffer)
{
    memset(buffer, '\0', MAX_BUFFER_LENGTH);
    return recv(*sockfd, buffer, MAX_BUFFER_LENGTH, 0);
}

SOCKET sendMessage(const SOCKET *const sockfd, const char *buffer)
{
    return send(*sockfd, buffer, strlen(buffer), 0);
}

extern void getMessage(char * buffer)
{
    memset(buffer, '\0', MAX_BUFFER_LENGTH);
    printf("What do you want to send (or exit)? ");
    readString(buffer, MAX_BUFFER_LENGTH);
    printf("\n");
}

extern void communication(const SOCKET *const sockfd,
                          const uint8_t * const moreThan1Flag)
{
    char buffer[MAX_BUFFER_LENGTH] = {0};
    int result = 0;
    if (*moreThan1Flag == 1) 
    {
        // If there are more than one thread, then send initial message.
        check(send(*sockfd, "Please", strlen("Please"), 0), "Sending message");
    }
    while (1)
    {
        result = receiveMessage(sockfd, buffer);
        if (result < 0) 
        {
            // If result == 0, then the server ended connection;
            // If result < 0, then there was a receive error.
            // Either way, end communication.
            printf("Receive failure.\n");
            break;
        }
        printf("\nReceived message: %s", buffer);
        if (buffer[result - 1] != '\n')
        {
            // If the message ending does not have endline symbol, then add it.
            printf("\n");
        }
        
        if (*moreThan1Flag == 0) 
        {
            // If there is only one thread, then allow console input sending.
            getMessage(buffer);
            if (strncmp(buffer, "exit", strlen("exit")) == 0) {
                printf("Client exit.\n");
                break;
            }
            // Send message and check if everything is okay.
            check(sendMessage(sockfd, buffer), "Sending message");
        }
        else if (*moreThan1Flag == 1)
        {
            // If there is more than one thread, then receive a single message
            // and end communication.
            break;
        }
    }
}

extern void sockClose(const SOCKET sock)
{
    int status = 0;
    #ifdef _WIN32
        status = shutdown(sock, SD_BOTH);
        if (status == 0) 
        {
            status = closesocket(sock);
            WSACleanup();
        }
    #else
        status = shutdown(sock, SHUT_RDWR);
        if (status == 0) 
        {
            status = close(sock);
        }
    #endif
}


extern void check(const SOCKET socket, const char *errorPlace)
{
    #ifdef _WIN32
        if ((int)socket == SOCKET_ERROR || socket == INVALID_SOCKET)
        {
            printf("%s: %d\n", errorPlace, WSAGetLastError());
            if ((int)socket == SOCKET_ERROR)
            {
                closesocket(socket);
            }
            WSACleanup();
            exit(1);
        }
    #else
        if (socket < 0)
        {
            printf("%s\n", errorPlace);
            exit(1);
        }
    #endif
}
