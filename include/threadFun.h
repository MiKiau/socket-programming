/**
 * @file threadFun.h
 * @brief Contains all thread function definitions. 
 * 
 * @author Žygimantas Kungelis
 */


#include <stdint.h>


#ifndef THREADHEADER
#define THREADHEADER

#ifdef _WIN32

/**
 * @brief Creates a socket communication with default IP and a given 
 * port server. 
 * 
 * @param arg The port to which the communication will take place.
 * @return Null pointer.
 */
extern unsigned __stdcall threadCommunication(void *arg);

#else

/**
 * @brief Creates a socket communication with default IP and a given 
 * port server. 
 * 
 * @param arg The port to which the communication will take place.
 * @return Null pointer.
 */
extern void *threadCommunication(void *arg);

#endif // _WIN32

/**
 * @brief Creates threads.
 * 
 * @param threadCount The number of threads to create;
 * @param portArray The ports containing array.
 * @return Void.
 */
extern void startThreadCreating(const int *const threadCount,
                                const uint16_t *portArray);

#endif // THREADHEADER