/**
 * @file threadFun.h
 * @brief Contains all thread function definitions. 
 * 
 * @author Žygimantas Kungelis
 */


#ifndef UTILH
#define UTILH

#include <stdint.h> // for uint16_t

/**
 * @brief Reads a string from stdin.
 * 
 * @param buffer The place, where the input will be saved.
 * @param size The size of buffer.
 * @return Void.
 */
extern void readString(char *buffer, const int size);

/**
 * @brief Reads an int from stdin.
 * 
 * @return The inputted number.
 */
extern int readInt(void);

extern void readPorts(uint16_t *portArray, const int *const threadCount);


#endif // UTILH