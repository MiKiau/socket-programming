/**
 * @file constants.h
 * @brief Contains all constants. 
 * 
 * @author Žygimantas Kungelis
 */


#ifndef CONSTANTS
#define CONSTANTS


#define SERVER_IP_ADDRESS "78.56.77.230"
#define MAX_BUFFER_LENGTH 1024
#define MAX_INPUT 10
#define MAX_THREAD_NUMBER 20


#endif // CONSTANTS