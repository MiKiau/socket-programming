/**
 * @file commFun.h
 * @brief Contains all socket communication function definitions. 
 * 
 * @author Žygimantas Kungelis
 */


#ifndef COMMFUN
#define COMMFUN

#include "main.h"
#include <stdint.h>


extern void sockInit(void); 

/**
 * @brief Creates a TCP/IP stream type socket.
 * 
 * @return Socket's file handler.
 */
extern SOCKET createSocket(void);

/**
 * @brief Connects socket to Server.
 * 
 * Detailed description starts here:
 * Connects to server, whose ip is SERVER_IP_ADDRESS, found in constants.h,
 * and port is gien as an argument.
 *  
 * @param sockfd A specific socket's file handler;
 * @param port Server's port number.
 * @return Void.
 */
extern void connectToServer(const SOCKET *const sockfd,
                            const uint16_t *const port);

/**
 * @brief Received a message from the server.
 * 
 * Detailed description starts here:
 * First, cleans up the buffer. Then receives a message, whose length could be
 * MAX_BUFFER_LENGTH found in constants.h.
 * 
 * @param sockfd A specific socket's file handler;
 * @param buffer A string, where the message will be saved.
 * @return Void.
 */
SOCKET receiveMessage(const SOCKET *const sockfd, char *buffer);

/**
 * @brief Sends message to server through socket.
 * 
 * @param sockfd A specific socket's file handler;
 * @param buffer The string of the message.
 * @return Void.
 */
SOCKET sendMessage(const SOCKET *const sockfd, const char *buffer);

extern void getMessage(char * buffer);

/**
 * @brief The main communication while loop.
 * 
 * Here the process of message receival and sending happens.
 * 
 * @param sockfd The specific socket's file handler through which the 
 * communication is taking place.
 * @return Void.
 */
extern void communication(const SOCKET *const sockfd,
                          const uint8_t * const moreThan1Flag);

/**
 * @brief Closes the communication through specific file handler.
 * 
 * @param sockfd A specific socket's file handler;
 * @return Void.
 */
extern void sockClose(const SOCKET sock);

/**
 * @brief Checks if the target is valid.
 * 
 * Detailed explanation starts here:
 * Checks if the target is negative, if yes, then the program will close;
 * 
 * @param target The integer to check.
 * @return Void.
 */
extern void check(const SOCKET socket, const char *errorPlace);

#endif // COMMFUN
