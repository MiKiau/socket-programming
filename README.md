# Socket programming

A program used to learn more about:
- Socket programming;
- Threads;
- CMake;
- Cross-platform programming.

# Requirements

1. CMake;
2. Git;
3. C compiler (for WINDOWS MinGW is preferable);
4. RECOMENDED: VS Code

# Instalation

For Unix systems:
1. git clone https://gitlab.com/MiKiau/socket-programming.git
2. cd socket-programming
3. mkdir build && cd build
4. cmake "Unix Makefiles" ..
5. ./socket-programming

For Windows systems:
1. git clone https://gitlab.com/MiKiau/socket-programming.git
2. cd socket-programming
3. mkdir build
4. cd build
5. cmake "MinGW makefiles" ..  <- if the MinGW compiler is used
6. .\socket-programming.exe

# Changelog

The final program must be able to:
1. Connect to a specific server (ip is given; port is also given) and communicate with it (send and receive messages);
2. Connect to a specific server's multiple ports (the task requested 3 connections), send "Please" once through all ports and receive a single message.

Also, the program must be cross-platform (able to be compiled in WIN and Unix OS) and it must be compiled using CMake.

## socket-programming-0.1v

The program should:
1. Successfully connect to the server and converse with it;
2. Successfully create more than 1 thread, which connect to the server through given ports;

## socket-programming-0.2v

The program should:
1. Successfully create threads and connect to the server in Linux and Windows OS.